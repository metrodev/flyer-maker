import React from 'react';
import PropTypes from 'prop-types';

const ColorPicker = ({ label, ...props }) => (
  <label>
    {label}
    <input type="color" {...props} />
  </label>
);

ColorPicker.defaultProps = {
  label: 'Selecciona un color'
};

ColorPicker.propTypes = {
  label: PropTypes.string.isRequired
}

export default ColorPicker;