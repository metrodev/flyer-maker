import React, { useState } from 'react';

import styles from './App.module.css';
import Canvas from './Canvas';
import ColorPicker from './ColorPicker';
import Logo from './mtro.png';

const compositeOpts = [ 'source-over','source-in','source-out','source-atop',
'destination-over','destination-in','destination-out','destination-atop',
'lighter', 'copy','xor', 'multiply', 'screen', 'overlay', 'darken',
'lighten', 'color-dodge', 'color-burn', 'hard-light', 'soft-light',
'difference', 'exclusion', 'hue', 'saturation', 'color', 'luminosity'
]

function App() {
  const [downloadLink, setDownloadLink] = useState('#');
  const [fill, setFill] = useState('#fff');
  const [logoScale, setLogoScale] = useState(0.3);
  const [partnerLogo, setPartnerLogo] = useState(null);
  const [ composite, setComposite ] = useState(compositeOpts[0]);
  const [partnerScale, setPartnerScale] = useState(1);

  return (
    <div className={styles.App}>
      <header className={styles['App-header']}>
        
        <h1><img className={styles.App__Logo} src={Logo} alt="Metro" /> FlyerMaker</h1>
      </header>

      <main className={styles.App__Grid}>
        <aside>
          <h3>Menu</h3>
          <div className={styles.App__Tool}>
            <ColorPicker onChange={(e) => setFill(e.target.value)} />
          </div>
          
          <label className={styles.App__Tool}>
            <b>Tamaño del logo</b>
            <input onChange={e => setLogoScale(e.target.value)} min={0} max={1} step={0.1} value={logoScale} type="range" />
          </label>

          <div className={styles.App__Tool}>
            <label>
              <b>Logo del patrocinador</b>
              <input accept="image/*" type="file" onChange={e => {
                if(e.target.files[0]) {
                  const url = URL.createObjectURL(e.target.files[0]);
                  setPartnerLogo(url);
                }
              }} />
            </label>

              { partnerLogo && <img src={partnerLogo} />}
              
              { partnerLogo && <button onClick={() => {
                setPartnerLogo(null);
                URL.revokeObjectURL(partnerLogo);
              }}>Eliminar logo</button>}
          </div>
          {
            partnerLogo && 
            <>
              <label className={styles.App__Tool}>
                  <b>Tamaño del logo</b>
                  <input onChange={e => setPartnerScale(e.target.value)} min={0} max={1} step={0.05} value={partnerScale} type="range" />
              </label>
              <label className={styles.App__Tool}>
                <b>Modo de combinacion</b>
                <select value={composite} onChange={e => setComposite(e.target.value)}>
                  { compositeOpts.map(o => <option value={o} key={o}>{o}</option>) }
                </select>
              </label>
            </>
          }
        </aside>
        
        <Canvas
          updateLink={link => {
            setDownloadLink(link);
            URL.revokeObjectURL(link);
          }}
          partnerScale={partnerScale}
          partnerComposite={composite}
          partnerLogo={partnerLogo}
          logoScale={logoScale}
          fill={fill} />

        <a href={downloadLink} download className={styles.App__Download}>
          Descargar
        </a>
      </main>
    </div>
  );
}

export default App;
