/**
 * @param {CanvasRenderingContext2D} ctx 
 */
function DrawConfetti(ctx) {
  const w = 7;
  const h = 10;
  const color = 0xFFFFFF;
  const t = ctx.getTransform();
  
  for(let i = 0; i < 250; i++) {
    ctx.setTransform(Math.random(), Math.random()*0.2, Math.random()*0.2, Math.random(), Math.random()*100, Math.random()*100);
    ctx.fillStyle =(`#${Math.floor(Math.random()*color).toString(16)}`)
    ctx.fillRect(Math.random()*400, Math.random()*400, w, h);
    ctx.globalAlpha = Math.max(0.85, Math.random());
  }

  ctx.globalAlpha = 1;
  ctx.setTransform(t);
}

export default DrawConfetti;