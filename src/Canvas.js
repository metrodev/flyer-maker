/**
 * The canvas
 */
import React, { useRef, useEffect, useState } from 'react';
import Logo from './CdlaLogo.png';

import styles from './Canvas.module.css';
import DrawConfetti from './Confetti';

const MAX_SIZE = 550;

const Canvas = ({ fill, logoScale, partnerLogo, partnerComposite, partnerScale, updateLink }) => {
  /**
   * @type {React.MutableRefObject<HTMLCanvasElement>}
   */
  const canvas = useRef(null);
  const [ size, setSize ] = useState(Math.min(MAX_SIZE, window.innerWidth));

  useEffect(() => {
    if (!canvas.current) return;

    const ctx = canvas.current.getContext('2d');

    ctx.fillStyle = fill;

    ctx.fillRect(0,0, size, size);

    const img = new Image();

    img.src = Logo;

    img.onload = () =>  {
      ctx.drawImage(img, (size - img.width*logoScale)/2, size - img.height*logoScale - 10, img.width*logoScale, img.height*logoScale);
      updateLink(canvas.current.toDataURL());
    };
    // drawImage(ctx, Logo, logoScale);

    DrawConfetti(ctx);

    if (partnerLogo) {
      const img = new Image();
      const scale = partnerScale;

      img.src = partnerLogo;
    
      img.onload = () =>  {
        ctx.imageSmoothingEnabled = true;
        ctx.imageSmoothingQuality = 'high';
        ctx.globalCompositeOperation = partnerComposite;
        
        ctx.shadowColor = '#000';
        ctx.shadowBlur = 15;

        ctx.drawImage(img, (size - img.width*scale)/2, (size - img.height*scale)/2, img.width*scale, img.height*scale);
        
        ctx.globalCompositeOperation = 'source-over';
        ctx.shadowColor = null;
        ctx.shadowBlur = 0;
        
        updateLink(canvas.current.toDataURL());
      }
    }

  }, [fill, logoScale, partnerLogo, partnerComposite, partnerScale]);

  useEffect(() => {
    function resize() {
      setSize(Math.min(MAX_SIZE, window.innerWidth))
    }

    window.addEventListener('resize', resize);

    return window.removeEventListener('resize', resize);
  }, [])

  return (
    <canvas style={{width: size, height: size}} className={styles.Canvas} width={size} height={size} ref={canvas} />
  )
};

Canvas.defaultProps = {
  fill: '#fff',
  logoScale: 1,
  partnerLogo: null,
  partnerComposite: 'source-over',
  partnerScale: 1,
  mask: '#fff'
}

export default Canvas;